#include <iostream>
using namespace std;

class Game
{
private:
	int number;
	int att;
public:
	Game() 
	{
		number = (rand() % 200) - 100;
		att = 0;
	}
	bool checkNumber(int num) {
		bool b = false;
		att++;
		if (number == num) {
			cout << "You guessed the number " << att << " attempts" << endl;
			b = true;
		}
		else if (num < number) 
			cout << "Your number is too small" << endl;
		else
			cout << "Your number too large" << endl;
		return b;
	}
};

int main() {
	int temp;
	Game obj;
	while (true) {
		cout << "Enter number" << endl;
		cin >> temp;
		if (obj.checkNumber(temp))
			break;
	}
	system("pause");
	return 0;
}